﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebColegio.Data.Entities;

namespace WebColegio.Models
{
    public class StudentEvaluationsViewModel
    {

        public int EvaluationId { get; set; }
        public int StudentId { get; set; }
        public Student Student { get; set; }

        public IEnumerable<SelectListItem> Evaluations { get; set; }
        public decimal Qualification { get; set; }

    }
}
