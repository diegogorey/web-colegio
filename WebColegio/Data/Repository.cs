﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebColegio.Data.Entities;

namespace WebColegio.Data
{
    public class Repository : IRepository
    {
        private readonly ApplicationDbContext _context;
        private readonly ILogger<Repository> logger;

        public Repository(ApplicationDbContext context, ILogger<Repository> logger)
        {
            this._context = context;
            this.logger = logger;
        }

        public void AddStudent(Student student)
        {
            _context.Add(student); 
        }

        public Student GetStudent(int id)
        {
            return _context.Students
                 .FirstOrDefault(m => m.Id == id);
        }

        public IEnumerable<Student> GetStudents()
        {
            return _context.Students.OrderBy(p => p.Name); 
        }

        public void RemoveStudent(Student student)
        {
            this._context.Remove(student);
        }

        public async Task<bool> SaveAllAsync()
        {
            try
            {
                return await this._context.SaveChangesAsync() > 0;
            }
            catch (Exception ex)
            {
                this.logger.LogError($"Something horrible has happened: {ex}");
                return false;
            }
        }

        public bool StudentExists(int id)
        {
           return _context.Students.Any(e => e.Id == id);
        }

        public void UpdateStudent(Student student)
        {
            _context.Students.Update(student);
        } 

        public Evaluation GetEvaluation(int id)
        {
            return _context.Evaluation.Where(p => p.Id ==id).FirstOrDefault();
        }
        public IEnumerable<Evaluation> GetEvaluations()
        {
            return _context.Evaluation.OrderBy(p => p.EvaluationDetail);
        }

        public void AddEvaluation(Evaluation evaluation)
        {
            this._context.Add(evaluation);
        }
        public void RemoveEvaluation(Evaluation evaluation)
        {
            this._context.Evaluation.Remove(evaluation);
        }
        public void UpdateEvaluation(Evaluation evaluation)
        {
            this._context.Evaluation.Update(evaluation);
        }
        public bool EvaluationExists(int id)
        {
            return _context.Evaluation.Any(e => e.Id == id);
        }

        public decimal SumEvaluationPercentage()
        {
            return _context.Evaluation.Sum(p => p.Percentage);
        }


        public void AddStudentEvaluation(StudentEvaluation studentEvaluation)
        {
            this._context.Add(studentEvaluation);
        }

        public IEnumerable<StudentEvaluation> GetStudentEvaluations()
        {
            return this._context.StudentEvaluation.Include(s => s.Student).Include(e => e.Evaluation);
        }

        public void RemoveStudentEvaluation(StudentEvaluation studentEvaluation)
        {
            this._context.StudentEvaluation.Remove(studentEvaluation);
        }

        public StudentEvaluation GetStudentEvaluation(int id)
        {
            return _context.StudentEvaluation.Where(p => p.Id == id).FirstOrDefault();

        }

        public int CountStudents()
        => this._context.Students.Count(); 
        public int CountEvaluations()
        => this._context.Evaluation.Count();
    }
}
