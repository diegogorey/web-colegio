﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebColegio.Data.Entities;

namespace WebColegio.Data
{
    public interface IRepository
    {
        IEnumerable<Student> GetStudents();
        void AddStudent(Student student);
        Student GetStudent(int id);
        void RemoveStudent(Student student);
        Task<bool> SaveAllAsync();
        void UpdateStudent(Student student);
        bool StudentExists(int id);

        Evaluation GetEvaluation(int id);
        IEnumerable<Evaluation> GetEvaluations();
        void AddEvaluation(Evaluation evaluation);
        void RemoveEvaluation(Evaluation evaluation);
        void UpdateEvaluation(Evaluation evaluation);
        bool EvaluationExists(int id);
        decimal SumEvaluationPercentage();


        void AddStudentEvaluation(StudentEvaluation studentEvaluation);
        IEnumerable<StudentEvaluation> GetStudentEvaluations();
        void RemoveStudentEvaluation(StudentEvaluation studentEvaluation);
        StudentEvaluation GetStudentEvaluation(int id);

        int CountStudents();
        int CountEvaluations();


    }
}
