﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebColegio.Data.Entities
{
    public class Student
    {

        public int Id { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido.")]
        [MaxLength(50)]
        [Display(Name = "Nombre")]
        public string Name { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido.")]
        [MaxLength(50)]
        [Display(Name = "Apellido")]
        public string LastName { get; set; }

        //[Required(ErrorMessage = "El campo {0} es requerido.")]
        [MaxLength(20)]
        [Display(Name = "Número Identificación")]
        public string Identification { get; set; }

        [Display(Name = "Codigo")]
        public string Codigo { get; set; }

        public IEnumerable<StudentEvaluation> StudentEvaluation { get; set; }
    }
}
