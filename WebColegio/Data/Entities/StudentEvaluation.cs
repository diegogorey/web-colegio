﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebColegio.Data.Entities
{
    public class StudentEvaluation
    {
        public int Id { get; set; }
        public Student Student { get; set; }
        public Evaluation Evaluation { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido.")]
        public decimal Qualification { get; set; }

    }
}
