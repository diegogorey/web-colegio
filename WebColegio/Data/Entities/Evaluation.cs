﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebColegio.Data.Entities
{
    public class Evaluation
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido.")]
        [Range(1, 25, ErrorMessage = "El porcentaje  debe ser entre 1 y 25 %")]
        [DisplayFormat(DataFormatString = "{0:C2}", ApplyFormatInEditMode = false)]
        public decimal Percentage { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido.")]
        public string EvaluationDetail { get; set; }

        public string Description { get; set; }


    }
}
