﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WebColegio.Data;
using WebColegio.Data.Entities;

namespace WebColegio.Controllers
{
    [Authorize]
    public class EvaluationsController : Controller
    {
         
        private readonly IRepository repository;
        public EvaluationsController(IRepository repository)
        {
            this.repository = repository;
        }

        // GET: Evaluations
        public IActionResult  Index()
        {
            return View(this.repository.GetEvaluations());
        }

        // GET: Evaluations/Details/5
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var evaluation = this.repository.GetEvaluation(id.Value);
            if (evaluation == null)
            {
                return NotFound();
            }

            return View(evaluation);
        }

        // GET: Evaluations/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Evaluations/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Evaluation evaluation)
        {
            if (ModelState.IsValid)
            {
                var sum = this.repository.SumEvaluationPercentage();

                var porcentaje = 100 - sum;
                  
                if (sum >= 100)
                {
                    ViewData["Message"] = "No se puede crear más evaluaciones ya se acredito el 100% ";

                }
                else if (evaluation.Percentage > porcentaje)
                {
                    ViewData["Message"] = "El porcentaje no puede ser superior al " + porcentaje + "%";

                }
                else
                {
                    this.repository.AddEvaluation(evaluation);
                    await this.repository.SaveAllAsync();
                    return RedirectToAction(nameof(Index));
                    
                }
                
            }
            return View(evaluation);
        }

        // GET: Evaluations/Edit/5
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var evaluation = this.repository.GetEvaluation(id.Value);
            if (evaluation == null)
            {
                return NotFound();
            }
            return View(evaluation);
        }

        // POST: Evaluations/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Percentage,EvaluationDetail,Description")] Evaluation evaluation)
        {
            if (id != evaluation.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    this.repository.UpdateEvaluation(evaluation);
                    await this.repository.SaveAllAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!EvaluationExists(evaluation.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(evaluation);
        }

        // GET: Evaluations/Delete/5
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var evaluation = this.repository.GetEvaluation(id.Value);
            if (evaluation == null)
            {
                return NotFound();
            }

            return View(evaluation);
        }

        // POST: Evaluations/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var evaluation = this.repository.GetEvaluation(id);
            this.repository.RemoveEvaluation(evaluation);
            await this.repository.SaveAllAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool EvaluationExists(int id)
        {
            return this.repository.EvaluationExists(id);
        }
    }
}
