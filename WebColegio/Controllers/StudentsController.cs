﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WebColegio.Data;
using WebColegio.Data.Entities;
using WebColegio.Models;

namespace WebColegio.Controllers
{
    [Authorize]
    public class StudentsController : Controller
    {

        private readonly IRepository repository;

        public StudentsController(IRepository repository)
        {
            this.repository = repository;
        }

        // GET: Students
        public IActionResult Index()
        {
            return View(this.repository.GetStudents());
        }

        // GET: Students/Details/5
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            
            var student = this.repository.GetStudent(id.Value);
            if (student == null)
            {
                return NotFound();
            }

            return View(student);
        }

        // GET: Students/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Students/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Student student)
        {
            if (ModelState.IsValid)
            {
                this.repository.AddStudent(student);
                await this.repository.SaveAllAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(student);
        }

        // GET: Students/Edit/5
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var student = this.repository.GetStudent(id.Value);
            if (student == null)
            {
                return NotFound();
            }
            return View(student);
        }

        // POST: Students/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id,  Student student)
        {
            if (id != student.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    this.repository.UpdateStudent(student);
              
                    await this.repository.SaveAllAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!StudentExists(student.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(student);
        }

        // GET: Students/Delete/5
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var student = this.repository.GetStudent(id.Value);
              
            if (student == null)
            {
                return NotFound();
            }

            return View(student);
        }

        // POST: Students/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var student = this.repository.GetStudent(id);
            this.repository.RemoveStudent(student);
            await this.repository.SaveAllAsync();
            return RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> DeleteStudentEvaluation(int id)
        {
            var studentEvaluation = this.repository.GetStudentEvaluation(id);
            this.repository.RemoveStudentEvaluation(studentEvaluation);
            await this.repository.SaveAllAsync();
            return RedirectToAction(nameof(StudentEvaluations));
        }



        private bool StudentExists(int id)
        {
            return this.repository.StudentExists(id);
        }

        public IActionResult AddEvaluation(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var student = this.repository.GetStudent(id.Value);

            var model = new StudentEvaluationsViewModel
            {
                Evaluations = this.GetEvaluationsList(), 
                Student = student
            };

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> AddEvaluation(StudentEvaluationsViewModel model)
        {
            if (ModelState.IsValid)
            {


                var evaluation = this.repository.GetEvaluation(model.EvaluationId);
                var student = this.repository.GetStudent(model.StudentId);

                this.repository.AddStudentEvaluation(
                        new StudentEvaluation
                        {
                            Evaluation = evaluation,
                            Qualification = model.Qualification,
                            Student = student

                        }

                    );
                await this.repository.SaveAllAsync();
                return RedirectToAction("StudentEvaluations");
            }

            return View(model);
        }

        public IActionResult StudentEvaluations()
        {
            var model = this.repository.GetStudentEvaluations();
            return View(model);
        }

        private IEnumerable<SelectListItem> GetEvaluationsList()
        {
            var evaluations = this.repository.GetEvaluations().ToList();
            evaluations.Insert(0, new Evaluation
            {
                Id = 0,
                EvaluationDetail = "(Seleccione una evaluación..)"
            });

            return evaluations.Select(p => new SelectListItem
            {
                Value = p.Id.ToString(),
                Text = p.EvaluationDetail
            }).ToList();
        }
         
    }
}
